import axios from "axios";

const CRYPTO_URL = process.env.REACT_APP_CRYPTO_URL

export default axios.create({
  baseURL: CRYPTO_URL,
  timeout: 5000,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }
})