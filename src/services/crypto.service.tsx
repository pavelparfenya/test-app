import api from "../api";

interface ICryptoService {
  getCurrencies: (currency: string) => Promise<any>
}

const cryptoService: ICryptoService = {
  getCurrencies: currency => api.get('/pricemultifull', {
    params: {
      fsyms: 'BTC,ETH,SOL,XRP,BNB,ADA,DOT,ATOM,LUNA,UNI',
      tsyms: currency
    }
  }).then(({ data }) => data)
}

export default cryptoService