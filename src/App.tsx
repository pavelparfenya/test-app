import React from 'react';
import { Layout } from 'antd';
import CurrencyTable from './components/CurrencyTable';

const { Content } = Layout;

function App() {
  return (
    <Layout>
      <Content style={{ padding: 20, height: '100vh' }}>
        <CurrencyTable />
      </Content>
  </Layout>
  );
}

export default App;
