const parseCurrenciesToArray = (data: any, currencyName: string) => {
  const currencies: any[] = []

  for(let currency in data.DISPLAY) {
    const temp: any = {}

    temp.FROMSYMBOL = data.RAW[currency][currencyName].FROMSYMBOL
    temp.PRICE = data.DISPLAY[currency][currencyName].PRICE
    temp.OPENDAY = data.DISPLAY[currency][currencyName].OPENDAY
    temp.CHANGEPCTDAY = `${data.DISPLAY[currency][currencyName].CHANGEPCTDAY}% (${data.DISPLAY[currency][currencyName].CHANGEDAY})`

    currencies.push(temp)
  }

  return currencies
}

export default parseCurrenciesToArray