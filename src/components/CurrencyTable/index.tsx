import React, { useEffect, useState } from "react";
import cryptoService from "../../services/crypto.service";
import parseCurrenciesToArray from "../../helpers/parseCurrenciesToArray";
import CurrencyTableView from "./view";

const CurrencyTable = () => {
	const [currencies, setCurrencies] = useState<any[]>([]);
	const [currency, setCurrency] = useState("USD");

	useEffect(() => {
		cryptoService
			.getCurrencies(currency)
			.then((data) => setCurrencies(parseCurrenciesToArray(data, currency)));
	}, [currency]);

	return (
		<CurrencyTableView
			setCurrency={setCurrency}
			currencies={currencies}
		/>
	);
};

export default CurrencyTable;
