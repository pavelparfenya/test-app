import React, { FC } from "react";
import { Table, Select } from "antd";

const columns = [
  {
    title: "Coin Name",
    dataIndex: "FROMSYMBOL",
    width: "30%",
  },
  {
    title: "Current Price (USD)",
    dataIndex: "PRICE",
  },
  {
    title: "Opening price (USD)",
    dataIndex: "OPENDAY",
  },
  {
    title: "Price Increase",
    dataIndex: "CHANGEPCTDAY",
    sorter: (a: any, b: any) =>
      a.CHANGEPCTDAY.split("%")[0] - b.CHANGEPCTDAY.split("%")[0],
  },
];

interface ICurrencyTableView {
  setCurrency: (currency: string) => void
  currencies: any[]
}

const CurrencyTableView: FC<ICurrencyTableView> = ({setCurrency, currencies}) => {
	const changeCurrency = (currency: string) => {
		setCurrency(currency);
	};

	return (
		<>
			<Select
				style={{ width: 200, marginBottom: 20 }}
				placeholder="Select currency"
				defaultValue="USD"
				onChange={changeCurrency}
			>
				<Select.Option value="USD">USD</Select.Option>
				<Select.Option value="EUR">EUR</Select.Option>
			</Select>

			<Table columns={columns} dataSource={currencies} />
		</>
	);
};

export default CurrencyTableView;
